## Chapter 4 – Lets Make a Plan

- [x] Clean unused files and folder
- [ ] Write down in Trello the Principal Features
- [ ] Order the features List
- [ ] Create Chapter Scripts files with the Trello Plan (just for Git visualization)
- [ ] Make a detail list for every feature
- [ ] composer require facade/ignition-code-editor --dev
- [ ] php artisan vendor:publish

