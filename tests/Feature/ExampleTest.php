<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function basic_test()
    {
        $user = factory(User::class)->create();
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
